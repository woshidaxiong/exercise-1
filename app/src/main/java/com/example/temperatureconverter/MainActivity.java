package com.example.temperatureconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private TextView textView;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioButton1 = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        button = findViewById(R.id.button2);
        textView = findViewById(R.id.textView);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float val = 0;
                if(textView.getText().toString().isEmpty()){
                    Toast.makeText( MainActivity.this, "Please enter a valid number",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!radioButton1.isChecked() && !radioButton2.isChecked()){
                    Toast.makeText( MainActivity.this, "Please choose a type of temperature",Toast.LENGTH_LONG).show();
                    return;
                }
                if(radioButton1.isChecked()){
                    val = Float.parseFloat(textView.getText().toString());
                    val = (val - 32)  * 5 / 9;
                    textView.setText(String.valueOf(val));
                    radioButton1.setChecked(false);
                    radioButton2.setChecked(true);
                }else{
                    val = Float.parseFloat(textView.getText().toString());
                    val = ((val * 9)  / 5)  + 32;
                    textView.setText(String.valueOf(val));
                    radioButton1.setChecked(true);
                    radioButton2.setChecked(false);
                }
            }
        });
    }
}